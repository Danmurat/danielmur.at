var gaProperty = 'UA-150233679-1';

// Disable tracking if the opt-out cookie exists.
var disableStr = 'ga-disable-' + gaProperty;
if (document.cookie.indexOf(disableStr + '=true') > -1) {
  window[disableStr] = true;
}
else{
    window[disableStr] = false;
}

// Opt-out function
function gaOptout() {
  document.cookie = disableStr + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
  window[disableStr] = true;
}

// Opt-in function
function gaOptin() {
    document.cookie = disableStr + '=false; expires=Thu, 01 Jan 1970 23:59:59 UTC; path=/';
    window[disableStr] = false;
}


$('#read_more_cookie').click(function(){

  gtag('event', 'Click', {
    'event_category': 'Button',
    'event_label': 'Read More Cookies',
    'send_to': 'UA-150233679-1'
  });

  $('.read-more-cookie').show(300);
  $('#read_more_cookie').hide();
});

$('#accept_cookie').click(function(){
  
  gtag('event', 'Click', {
    'event_category': 'Button',
    'event_label': 'Accept Cookies',
    'send_to': 'UA-150233679-1'
  });

  gaOptin();
  $('.cookie-policy-container').animate(1000).hide();
});

$('.close').click(function(){
  
  gtag('event', 'Click', {
    'event_category': 'Button',
    'event_label': 'Close Cookies',
    'send_to': 'UA-150233679-1'
  });

  gaOptin();
  $('.cookie-policy-container').animate(1000).hide();
});

$('#reject_cookie').click(function(){

  gtag('event', 'Click', {
    'event_category': 'Button',
    'event_label': 'Reject Cookies',
    'send_to': 'UA-150233679-1'
  });

  $('.read-more-cookie').hide(300);
  $('#read_more_cookie').show();
  gaOptout();
  $('.cookie-policy-container').animate(1000).hide();
});


