(function($) {
  "use strict"; // Start of use strict

  // Smooth scrolling using jQuery easing
  $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: (target.offset().top - 71)
        }, 1000, "easeInOutExpo");
        return false;
      }
    }
  });

  // Scroll to top button appear
  $(document).scroll(function() {
    var scrollDistance = $(this).scrollTop();
    if (scrollDistance > 100) {
      $('.scroll-to-top').fadeIn();
    } else {
      $('.scroll-to-top').fadeOut();
    }
  });

  // Closes responsive menu when a scroll trigger link is clicked
  $('.js-scroll-trigger').click(function() {
    $('.navbar-collapse').collapse('hide');
  });

  // Activate scrollspy to add active class to navbar items on scroll
  $('body').scrollspy({
    target: '#mainNav',
    offset: 80
  });

  // Collapse Navbar
  var navbarCollapse = function() {
    if ($("#mainNav").offset().top > 100) {
      $("#mainNav").addClass("navbar-shrink");
    } else {
      $("#mainNav").removeClass("navbar-shrink");
    }
  };
  // Collapse now if page is not at top
  navbarCollapse();
  // Collapse the navbar when page is scrolled
  $(window).scroll(navbarCollapse);


})(jQuery); // End of use strict

function presentation(){

  if($('html, body').scrollTop() < 100){
    $('html, body').animate({scrollTop: $(document).height()-$(window).height()}, 40000, "linear");
  }
  else{
      $('html, body').animate({scrollTop: 0}, 40000, "linear");
  }
  
}

var startPresentation;
$('.navbar-brand').click(function(){
  startPresentation = window.setInterval(presentation, 42000);
  console.log('start');
});

$('.navbar-brand').on("contextmenu", function(){
  window.clearInterval(startPresentation);
  console.log('stop');
});

var num = 0;
function revolver(){
  if(num == 3){num=0}
  var sentences = ['solve customer problems', 'test incredible ideas', 'develop amazing products'];
  var theClass = ['text-danger', 'text-warning', 'text-success'];

  $('.revolver').removeClass('text-warning text-danger text-success');
  $('.revolver').text(sentences[num]).addClass(theClass[num]);
  num++
}

window.setInterval(revolver, 4000);