gsap.registerPlugin(ScrollTrigger);

//Table contents
gsap.from('.skill',{duration: 1, stagger:0.2, x: '-130%', ease:'back.in'});
gsap.from('.skillContainer', {duration: 1,  x: '-130%', ease:'back.in'});

let mm = gsap.matchMedia();

mm.add("(min-width: 800px)", () => {
    gsap.from('.skillRight',{duration: 1, stagger:0.2, x: '130%', ease:'back.in'});
    gsap.from('.skillContainerRight', {duration: 1,  x: '130%', ease:'back.in'});
    //Download Button
    gsap.from('.dl', {duration: 0.5, x: '100vh', delay: 1, ease:'back'});
    gsap.from('.footer', {duration: 2,  x: '-130%', ease:'back.in'});
});

mm.add("(max-width: 799px)", () => {
    var tl = gsap.timeline({
        scrollTrigger: {
          trigger: '.app',
          scrub: true,
          start: 'top',
          end: 'bottom-=250px bottom', //end 50px more than reduction in header height
        //   markers: true
        },
        defaults: { duration: 1 } // This gets passed to each tween
      });
    //Download Button
    tl.to('.dl', { opacity:0}).to('.container', {height: '-=250px'}, '<');
    tl.from('.skillContainerRight', {x: '130%', ease:'back.in'}, '<');
    tl.from('.skillRight', {stagger: 0.1, x: '130%', ease:'back.in'}, '<');

    gsap.from('.footer', {duration:2, x: '-130%', ease:'back.in', delay:3, scrollTrigger: {
        trigger: '.skillContainerRight',
        scrub: true,
        start: 'top-=150px',
        end: 'bottom-=100px bottom',
    }});
});

//Headings
gsap.from('.heading', {duration: 1, y: '-100vh', delay: 0.3, ease:'back'});
gsap.from('.subheading, .linkedIn', {duration: 2, opacity: '0', delay: 0.5, ease:'back.in'});
gsap.to('.container', {duration: 0.1, borderBottom: '1px solid #dddddd', delay: 3});