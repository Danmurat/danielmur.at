gsap.registerPlugin(ScrollTrigger);

//Table contents
// gsap.from('.grid-item',{duration: 5, stagger:0.2, x: '-130%', ease:'back.in'});
// gsap.from('.skillContainer', {duration: 1,  x: '-130%', ease:'back.in'});

let numbers = [];
function getRandomItem(arr) {
  const randomItem = Math.floor(Math.random() * arr.length);
  return arr[randomItem];
}

const totalNums = 90;
const container = document.getElementById('gridContainer');

for (let i = 1; i <= totalNums; i++) {
    //Push list of numbs to array
    numbers.push(i);

    const div = document.createElement('div');
    div.classList.add('grid-item');
    div.id = i;
    div.textContent = i; // Add the number as text
    container.appendChild(div);
}

function hideButton(){
  gsap.to('#drawNum',{duration: 0, visibility: 'hidden', ease:'back.in', delay: 0});
  gsap.to('#drawNum',{duration: 1, visibility: 'visible', ease:'back.in', delay: 3.5});
}

function updateShowNum(num){
  hideButton();
  document.getElementById('chosen-num').textContent = String(num);
  
  TweenMax.to(".santaImg", 0.1, {x:"+=20", keyframes: {
    rotation: [10,-8,6,-5,3,-1,0]
  }, yoyo:true, repeat:5});

  gsap.to('.chosen-number',{duration: 0.5, opacity: 1, ease:'back.in'});
  gsap.to('.chosen-number',{duration: 3, opacity: 0, ease:'back.in', delay: 0.5});

}

gsap.from('.grid-item',{duration: 0.5, stagger:0.1, opacity:0, ease:'back.in'});

document.getElementById('drawNum').onclick = function() {
  const randomItem = getRandomItem(numbers);
  document.getElementById(randomItem).classList.add('selected-number');

  numbers = numbers.filter(item => item !== randomItem);
  updateShowNum(randomItem);
  speakNumber(randomItem);

}

//SET UP VOICE SYNTH AND CHANGE VOICE
const synth = window.speechSynthesis;
let voices = window.speechSynthesis.getVoices();
// const dropdown = document.getElementById('voicesList');

// window.speechSynthesis.onvoiceschanged = function() {
//   voices = window.speechSynthesis.getVoices();
//   speak.voice = voices[1];
//   console.log(voices)
//   for(var i = 0; i < voices.length; ++i) {
//     var option = document.createElement('option');
//     option.text = voices[i].name;
//     option.value = i;
//     dropdown.add(option, 0);
// }
// };

// speak.voice = voices[1];
//SET UP VOICE SYNTH AND CHANGE VOICE
const speak = new SpeechSynthesisUtterance();

function speakNumber(number){
  // speak.voice = voices[dropdown.value];
  // speak.voice = voices[0];
  const thenumber = number.toString();
  const digits = thenumber.split("");
  
  if(digits.length <= 1){
    speak.text = `On it's own, number ${thenumber}`;
  }
  else{
    let extras = "";
    let extrasEnd = "";
    switch (thenumber){
      case "69":
        extrasEnd = "  - oy oy whoop whoop";
        break;
      case "12":
        extras = "  - One Dozen ";
        break;
      case "13":
        extras = "  - Unlucky for some ";
        break;
      case "15":
        extras = "  - Young and keen ";
        break;
      case "16":
        extras = "  - Sweet";
        break;
      case "21":
        extras = "  - Key in the door ";
        break;
      case "22":
        extras = "  - Two little ducks ";
        break;
      case "24":
        extras = "  - Two Dozen ";
        break;
      case "25":
        extras = "  - Duck and dive ";
        break;
      case "26":
        extras = "  - Pick and mix ";
        break;
      case "38":
        extras = "  - Christmas Cake ";
        break;
      case "66":
        extras = "  - Clickety Click ";
        break;
      case "88":
        extras = "  - Two fat ladies ";
        break;
      case "11":
        extras = "  - Legs ";
        break;
      default:
        break;
    }
    speak.text = `${digits[0]} and ${digits[1]}, ${extras} ${thenumber} ${extrasEnd}`;
  }
  
  synth.speak(speak);
}